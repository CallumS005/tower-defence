def IsColliding(a, b):
    if a.posX <= b.posX + b.width and b.posX <= a.posX + a.width and a.posY <= b.posY + b.height and b.posY <= a.posY + a.height:
        return True
    else:
        return False


def IsInRange(a, b, r):
    xdif = a[0] - b[0]
    ydif = a[1] - b[1]

    if (xdif <= r and xdif >= -r) and (ydif <= r and ydif >= -r):
        return True
    else:
        return False


