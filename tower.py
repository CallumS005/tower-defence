import time
from entity import Entity


class Tower(Entity):
    def __init__(self, x, y, w, h, attackDamage, attackRange, attackSpeed,
                 cost):
        Entity.__init__(self, x, y, w, h)
        self.attackDamage = attackDamage
        self.attackRange = attackRange
        self.attackSpeed = attackSpeed
        self.cost = cost
        self.nextAttackTime = int(time.time())
