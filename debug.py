import pygame

points = []
DEBUG = False


def DebugDrawPoints(display):
    # Creates red circles at the points set out by the AI path generator
    if not DEBUG:
        return

    for p in points:
        pygame.draw.circle(display, (255, 0, 0), p, 20, 0)


def DebugEnd():
    if not DEBUG:
        return

    if len(points) > 0:
        print(points)


def DebugInput(e):
    if not DEBUG:
        return

    if e.type == pygame.MOUSEBUTTONDOWN:
        if (e.button == 1):
            points.append(e.pos)
