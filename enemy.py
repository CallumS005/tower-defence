from entity import Entity


class Enemy(Entity):
    def __init__(self, maxHealth, x, y, w, h, attackDamage, attackRange):
        Entity.__init__(self, x, y, w, h)
        self.maxHealth = maxHealth
        self.health = self.maxHealth
        self.animation = "OrkIdle"
        self.animationID = 0
        self.targetID = 0
        self.state = "Walk"
        self.speed = 2
        self.flip = False

        self.attackDamage = attackDamage
        self.attackRange = attackRange

    def Update(self):
        if self.state == "Idle":
            self.animation = "OrkIdle"
        elif self.state == "Walk":
            if self.speed > 1:
                self.animation = "OrkRun"
            else:
                self.animation = "OrkWalk"
        elif self.state == "Attack":
            self.animation = "OrkAttack"
