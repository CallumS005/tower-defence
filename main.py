import pygame
import time
from debug import DebugDrawPoints, DebugEnd, DebugInput
from maths import IsInRange
from enemy import Enemy
from animation import Animations
from window import Window
from ai import aiPath, WalkAI
from tower import Tower

pygame.init()
textFont = pygame.font.SysFont("Ubuntu", 32)
running = True

# Array of enemies in the game
enemies = []
enemyPast = 0
towers = []


# ----------------------------------- GRAPHICS --------------------------------
class Renderer:
    def __init__(self, window):
        self.window = window

    def CreatePGDisplay(self):
        self.display = pygame.display.set_mode((self.window.width,
                                                self.window.height))
        pygame.display.set_caption(self.window.title)

    def Render(self):
        renderer.display.blit(background, (0, 0))
        renderer.display.blit(textFont.render("Tower Defense", True, (0, 0, 0)), (renderer.window.width / 2 - 85, 0))
        renderer.display.blit(textFont.render("This is an error message", True, (140, 18, 40)), (renderer.window.width / 2 - 85, 50))
        # Creates the sprites for all entities depending on what animation and
        # stage should be shown
        for e in enemies:
            a = animations.GetAnimationByName(e.animation)

            if a:
                if not e.flip:
                    self.display.blit(a.keyframes[e.animationID],
                                      (e.posX - (e.width / 2), e.posY -
                                       (e.height / 2)))
                else:
                    flip = pygame.transform.flip(a.keyframes[e.animationID],
                                                 True, False)
                    self.display.blit(flip,
                                      (e.posX - (e.width / 2), e.posY -
                                       (e.height / 2)))

        for tower in towers:
            pygame.draw.circle(self.display, (153, 102, 0), (tower.posX,
                               tower.posY), 30, 0)

        DebugDrawPoints(self.display)

        pygame.display.flip()


window = Window(1600, 800, "Tower Defence")
renderer = Renderer(window)
renderer.CreatePGDisplay()
animations = Animations()

# Loads and creates the background
background = pygame.image.load('assets/background.png').convert()
background = pygame.transform.smoothscale(background,
                                          renderer.display.get_size())

# --------------------------------- GAME LOGIC --------------------------------

# Tracks how many frames have passed since the last set cycle
cycle = 0
textCycle = 0

# Is called at the end of every cycle
def NextSecond():
    for enemy in enemies:
        if enemy.animationID == len(animations.runOrkSprites.keyframes) - 1:
            enemy.animationID = 0
        else:
            enemy.animationID = enemy.animationID + 1


def StartWave(count, spaceBetween=200):
    for i in range(count):
        e = Enemy(100, aiPath[0][0] - (i * spaceBetween), aiPath[0][1], 80, 80,
                  5, 100)
        enemies.append(e)


while running:
    for e in enemies:
        # Removes the enemy from the list when the health goes <= 0
        if e.health <= 0:
            enemies.remove(e)

        # Calculates how many enemies manage to get past the defences
        if e.targetID == (len(aiPath) - 1):
            enemyPast = enemyPast + 1
            enemies.remove(e)

        e.Update()

        if (e.state == "Walk"):
            WalkAI(e)

    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN:
            if (e.key == pygame.K_SPACE):
                StartWave(10)
        if e.type == pygame.MOUSEBUTTONDOWN:
            # TESTING
            if e.button == 1:
                success = True

                for et in towers:
                    if IsInRange(e.pos, et.GetPosition(), 250):
                        success = False

                if success:
                    t = Tower(e.pos[0], e.pos[1], 40, 40, 40, 300, 5, 200)
                    towers.append(t)
        DebugInput(e)

    if cycle == 3:
        cycle = -1
        NextSecond()

    if textCycle == 300:
        textCycle = -1
        print("T")
    # Loops through all the towers and enemies, to see if an enemy is within
    # the range of the tower to attack
    for tower in towers:
        for enemy in enemies:
            if IsInRange(enemy.GetPosition(), tower.GetPosition(), tower.attackRange):
                # If the tower attacks, wait until the current time +
                # attackSpeed (s) before attacking again
                if int(time.time()) >= tower.nextAttackTime:
                    enemy.health = enemy.health - tower.attackDamage
                    tower.nextAttackTime = int(time.time()) + tower.attackSpeed

    renderer.Render()
    window.clock.tick(60)
    cycle = cycle + 1
    textCycle = textCycle + 1

DebugEnd()
pygame.quit()
