import pygame


class Animation:
    def __init__(self, animationFiles, width, height):
        self.keyframes = []
        self.animationFiles = animationFiles
        self.width = width
        self.height = height

        for fileName in animationFiles:
            self.keyframes.append(pygame.image.load(fileName).convert_alpha())

        x = 0
        for sprite in self.keyframes:
            self.keyframes[x] = pygame.transform.smoothscale(sprite, (self.width, self.height))
            x = x + 1


class Animations:
    def __init__(self):
        self.idleOrkSprites = Animation([
            "assets/enemies/1_ORK/IDLE/IDLE_000.png",
            "assets/enemies/1_ORK/IDLE/IDLE_001.png",
            "assets/enemies/1_ORK/IDLE/IDLE_002.png",
            "assets/enemies/1_ORK/IDLE/IDLE_003.png",
            "assets/enemies/1_ORK/IDLE/IDLE_004.png",
            "assets/enemies/1_ORK/IDLE/IDLE_005.png",
            "assets/enemies/1_ORK/IDLE/IDLE_006.png",
            ], 80, 80)
        self.walkOrkSprites = Animation([
            "assets/enemies/1_ORK/WALK/WALK_000.png",
            "assets/enemies/1_ORK/WALK/WALK_001.png",
            "assets/enemies/1_ORK/WALK/WALK_002.png",
            "assets/enemies/1_ORK/WALK/WALK_003.png",
            "assets/enemies/1_ORK/WALK/WALK_004.png",
            "assets/enemies/1_ORK/WALK/WALK_005.png",
            "assets/enemies/1_ORK/WALK/WALK_006.png",
            ], 80, 80)
        self.runOrkSprites = Animation([
            "assets/enemies/1_ORK/RUN/RUN_000.png",
            "assets/enemies/1_ORK/RUN/RUN_001.png",
            "assets/enemies/1_ORK/RUN/RUN_002.png",
            "assets/enemies/1_ORK/RUN/RUN_003.png",
            "assets/enemies/1_ORK/RUN/RUN_004.png",
            "assets/enemies/1_ORK/RUN/RUN_005.png",
            "assets/enemies/1_ORK/RUN/RUN_006.png",
            ], 80, 80)
        self.attackOrkSprites = Animation([
            "assets/enemies/1_ORK/ATTAK/ATTAK_000.png",
            "assets/enemies/1_ORK/ATTAK/ATTAK_001.png",
            "assets/enemies/1_ORK/ATTAK/ATTAK_002.png",
            "assets/enemies/1_ORK/ATTAK/ATTAK_003.png",
            "assets/enemies/1_ORK/ATTAK/ATTAK_004.png",
            "assets/enemies/1_ORK/ATTAK/ATTAK_005.png",
            "assets/enemies/1_ORK/ATTAK/ATTAK_006.png",
            ], 80, 80)

    def GetAnimationByName(self, animation):
        if animation == "OrkIdle":
            return self.idleOrkSprites
        elif animation == "OrkWalk":
            return self.walkOrkSprites
        elif animation == "OrkRun":
            return self.runOrkSprites
        elif animation == "OrkAttack":
            return self.attackOrkSprites

        print(f"No animation with the name \"{animation}\"")
        return
